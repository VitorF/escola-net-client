package br.com.escolanet.util;

import br.com.ma.secretaria.funcionario.Funcionario;

public class MsgUtil {
	
	
	public static String msgCadastro( Funcionario funcionario , String link) {
		String msg = "\r\n" + 
				"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n" + 
				"<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n" + 
				"<head>\r\n" + 
				"  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n" + 
				"  <meta name=\"viewport\" content=\"width=device-width\" />\r\n" + 
				"  <title>Airmail Ping</title>\r\n" + 
				"  <style type=\"text/css\">\r\n" + 
				"\r\n" + 
				"  * {\r\n" + 
				"    margin:0;\r\n" + 
				"    padding:0;\r\n" + 
				"    font-family: Helvetica, Arial, sans-serif;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  img {\r\n" + 
				"    max-width: 100%;\r\n" + 
				"    outline: none;\r\n" + 
				"    text-decoration: none;\r\n" + 
				"    -ms-interpolation-mode: bicubic;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .image-fix {\r\n" + 
				"    display:block;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .collapse {\r\n" + 
				"    margin:0;\r\n" + 
				"    padding:0;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  body {\r\n" + 
				"    -webkit-font-smoothing:antialiased;\r\n" + 
				"    -webkit-text-size-adjust:none;\r\n" + 
				"    width: 100%!important;\r\n" + 
				"    height: 100%;\r\n" + 
				"    text-align: center;\r\n" + 
				"    color: #747474;\r\n" + 
				"    background-color: #ffffff;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  h1,h2,h3,h4,h5,h6 {\r\n" + 
				"    font-family: Helvetica, Arial, sans-serif;\r\n" + 
				"    line-height: 1.1;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  h1 small, h2 small, h3 small, h4 small, h5 small, h6 small {\r\n" + 
				"    font-size: 60%;\r\n" + 
				"    line-height: 0;\r\n" + 
				"    text-transform: none;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  h1 {\r\n" + 
				"    font-weight:200;\r\n" + 
				"    font-size: 44px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  h2 {\r\n" + 
				"    font-weight:200;\r\n" + 
				"    font-size: 32px;\r\n" + 
				"    margin-bottom: 14px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  h3 {\r\n" + 
				"    font-weight:500;\r\n" + 
				"    font-size: 27px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  h4 {\r\n" + 
				"    font-weight:500;\r\n" + 
				"    font-size: 23px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  h5 {\r\n" + 
				"    font-weight:900;\r\n" + 
				"    font-size: 17px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  h6 {\r\n" + 
				"    font-weight:900;\r\n" + 
				"    font-size: 14px;\r\n" + 
				"    text-transform: uppercase;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .collapse {\r\n" + 
				"    margin:0!important;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  td, div {\r\n" + 
				"    font-family: Helvetica, Arial, sans-serif;\r\n" + 
				"    text-align: center;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  p, ul {\r\n" + 
				"    margin-bottom: 10px;\r\n" + 
				"    font-weight: normal;\r\n" + 
				"    font-size:14px;\r\n" + 
				"    line-height:1.6;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  p.lead {\r\n" + 
				"    font-size:17px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  p.last {\r\n" + 
				"    margin-bottom:0px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  ul li {\r\n" + 
				"    margin-left:5px;\r\n" + 
				"    list-style-position: inside;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  a {\r\n" + 
				"    color: #747474;\r\n" + 
				"    text-decoration: none;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  a img {\r\n" + 
				"    border: none;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .head-wrap {\r\n" + 
				"    width: 100%;\r\n" + 
				"    margin: 0 auto;\r\n" + 
				"    background-color: #f9f8f8;\r\n" + 
				"    border-bottom: 1px solid #d8d8d8;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .head-wrap * {\r\n" + 
				"    margin: 0;\r\n" + 
				"    padding: 0;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .header-background {\r\n" + 
				"    background: repeat-x url(https://www.filepicker.io/api/file/wUGKTIOZTDqV2oJx5NCh) left bottom;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .header {\r\n" + 
				"    height: 42px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .header .content {\r\n" + 
				"    padding: 0;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .header .brand {\r\n" + 
				"    font-size: 16px;\r\n" + 
				"    line-height: 42px;\r\n" + 
				"    font-weight: bold;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .header .brand a {\r\n" + 
				"    color: #464646;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .body-wrap {\r\n" + 
				"    width: 505px;\r\n" + 
				"    margin: 0 auto;\r\n" + 
				"    background-color: #ffffff;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .soapbox .soapbox-title {\r\n" + 
				"    font-size: 21px;\r\n" + 
				"    color: #464646;\r\n" + 
				"    padding-top: 35px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .content .status-container.single .status-padding {\r\n" + 
				"    width: 80px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .content .status {\r\n" + 
				"    width: 90%;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .content .status-container.single .status {\r\n" + 
				"    width: 300px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .status {\r\n" + 
				"    border-collapse: collapse;\r\n" + 
				"    margin-left: 15px;\r\n" + 
				"    color: #656565;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .status .status-cell {\r\n" + 
				"    border: 1px solid #b3b3b3;\r\n" + 
				"    height: 50px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .status .status-cell.success,\r\n" + 
				"  .status .status-cell.active {\r\n" + 
				"    height: 65px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .status .status-cell.success {\r\n" + 
				"    background: #f2ffeb;\r\n" + 
				"    color: #51da42;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .status .status-cell.success .status-title {\r\n" + 
				"    font-size: 15px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .status .status-cell.active {\r\n" + 
				"    background: #fffde0;\r\n" + 
				"    width: 135px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .status .status-title {\r\n" + 
				"    font-size: 16px;\r\n" + 
				"    font-weight: bold;\r\n" + 
				"    line-height: 23px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .status .status-image {\r\n" + 
				"    vertical-align: text-bottom;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .body .body-padded,\r\n" + 
				"  .body .body-padding {\r\n" + 
				"    padding-top: 34px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .body .body-padding {\r\n" + 
				"    width: 41px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .body-padded,\r\n" + 
				"  .body-title {\r\n" + 
				"    text-align: left;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .body .body-title {\r\n" + 
				"    font-weight: bold;\r\n" + 
				"    font-size: 17px;\r\n" + 
				"    color: #464646;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .body .body-text .body-text-cell {\r\n" + 
				"    text-align: left;\r\n" + 
				"    font-size: 14px;\r\n" + 
				"    line-height: 1.6;\r\n" + 
				"    padding: 9px 0 17px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .body .body-text-cell a {\r\n" + 
				"    color: #464646;\r\n" + 
				"    text-decoration: underline;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .body .body-signature-block .body-signature-cell {\r\n" + 
				"    padding: 25px 0 30px;\r\n" + 
				"    text-align: left;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .body .body-signature {\r\n" + 
				"    font-family: \"Comic Sans MS\", Textile, cursive;\r\n" + 
				"    font-weight: bold;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .footer-wrap {\r\n" + 
				"    width: 100%;\r\n" + 
				"    margin: 0 auto;\r\n" + 
				"    clear: both !important;\r\n" + 
				"    background-color: #e5e5e5;\r\n" + 
				"    border-top: 1px solid #b3b3b3;\r\n" + 
				"    font-size: 12px;\r\n" + 
				"    color: #656565;\r\n" + 
				"    line-height: 30px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .footer-wrap .container {\r\n" + 
				"    padding: 14px 0;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .footer-wrap .container .content {\r\n" + 
				"    padding: 0;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .footer-wrap .container .footer-lead {\r\n" + 
				"    font-size: 14px;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .footer-wrap .container .footer-lead a {\r\n" + 
				"    font-size: 14px;\r\n" + 
				"    font-weight: bold;\r\n" + 
				"    color: #535353;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .footer-wrap .container a {\r\n" + 
				"    font-size: 12px;\r\n" + 
				"    color: #656565;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .footer-wrap .container a.last {\r\n" + 
				"    margin-right: 0;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .footer-wrap .footer-group {\r\n" + 
				"    display: inline-block;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .container {\r\n" + 
				"    display: block !important;\r\n" + 
				"    max-width: 505px !important;\r\n" + 
				"    clear: both !important;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .content {\r\n" + 
				"    padding: 0;\r\n" + 
				"    max-width: 505px;\r\n" + 
				"    margin: 0 auto;\r\n" + 
				"    display: block;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  .content table {\r\n" + 
				"    width: 100%;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"  .clear {\r\n" + 
				"    display: block;\r\n" + 
				"    clear: both;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  table.full-width-gmail-android {\r\n" + 
				"    width: 100% !important;\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  </style>\r\n" + 
				"\r\n" + 
				"  <style type=\"text/css\" media=\"only screen\">\r\n" + 
				"\r\n" + 
				"  @media only screen {\r\n" + 
				"\r\n" + 
				"    table[class*=\"head-wrap\"],\r\n" + 
				"    table[class*=\"body-wrap\"],\r\n" + 
				"    table[class*=\"footer-wrap\"] {\r\n" + 
				"      width: 100% !important;\r\n" + 
				"    }\r\n" + 
				"\r\n" + 
				"    td[class*=\"container\"] {\r\n" + 
				"      margin: 0 auto !important;\r\n" + 
				"    }\r\n" + 
				"\r\n" + 
				"  }\r\n" + 
				"\r\n" + 
				"  @media only screen and (max-width: 505px) {\r\n" + 
				"\r\n" + 
				"    *[class*=\"w320\"] {\r\n" + 
				"      width: 320px !important;\r\n" + 
				"    }\r\n" + 
				"\r\n" + 
				"    table[class=\"soapbox\"] td[class*=\"soapbox-title\"],\r\n" + 
				"    table[class=\"body\"] td[class*=\"body-padded\"] {\r\n" + 
				"      padding-top: 24px;\r\n" + 
				"    }\r\n" + 
				"  }\r\n" + 
				"  </style>\r\n" + 
				"</head>\r\n" + 
				"\r\n" + 
				"<body bgcolor=\"#ffffff\">\r\n" + 
				"\r\n" + 
				"  <div align=\"center\">\r\n" + 
				"    <table class=\"head-wrap w320 full-width-gmail-android\" bgcolor=\"#f9f8f8\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\r\n" + 
				"      <tr>\r\n" + 
				"        <td background=\"https://www.filepicker.io/api/file/UOesoVZTFObSHCgUDygC\" bgcolor=\"#ffffff\" width=\"100%\" height=\"8\" valign=\"top\">\r\n" + 
				"          <!--[if gte mso 9]>\r\n" + 
				"          <v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"mso-width-percent:1000;height:8px;\">\r\n" + 
				"            <v:fill type=\"tile\" src=\"https://www.filepicker.io/api/file/UOesoVZTFObSHCgUDygC\" color=\"#ffffff\" />\r\n" + 
				"            <v:textbox inset=\"0,0,0,0\">\r\n" + 
				"          <![endif]-->\r\n" + 
				"          <div height=\"8\">\r\n" + 
				"          </div>\r\n" + 
				"          <!--[if gte mso 9]>\r\n" + 
				"            </v:textbox>\r\n" + 
				"          </v:rect>\r\n" + 
				"          <![endif]-->\r\n" + 
				"        </td>\r\n" + 
				"      </tr>\r\n" + 
				"      <tr class=\"header-background\">\r\n" + 
				"        <td class=\"header container\" align=\"center\">\r\n" + 
				"          <div class=\"content\">\r\n" + 
				"            <span class=\"brand\">\r\n" + 
				"              <a href=\"#\">\r\n" + 
				"                ESCOLANET\r\n" + 
				"              </a>\r\n" + 
				"            </span>\r\n" + 
				"          </div>\r\n" + 
				"        </td>\r\n" + 
				"      </tr>\r\n" + 
				"    </table>\r\n" + 
				"\r\n" + 
				"    <table class=\"body-wrap w320\">\r\n" + 
				"      <tr>\r\n" + 
				"        <td></td>\r\n" + 
				"        <td class=\"container\">\r\n" + 
				"          <div class=\"content\">\r\n" + 
				"            <table cellspacing=\"0\">\r\n" + 
				"              <tr>\r\n" + 
				"                <td>\r\n" + 
				"                  <table class=\"soapbox\">\r\n" + 
				"                    <tr>\r\n" + 
				"                      <td class=\"soapbox-title\">Cadastro de Usuario</td>\r\n" + 
				"                    </tr>\r\n" + 
				"                  </table>\r\n" + 
				"                  <table class=\"body\">\r\n" + 
				"                    <tr>\r\n" + 
				"                      <td class=\"body-padding\"></td>\r\n" + 
				"                      <td class=\"body-padded\">\r\n" + 
				"                        <div class=\"body-title\">Ol�, "+funcionario.getNome()+"</div>\r\n" + 
				"                        <table class=\"body-text\">\r\n" + 
				"\r\n" + 
				"                            <p class=\"body-text-cell\">\r\n" + 
				"                                <b>Esse link se expira em 1 dia  , ap�is esse periodo o link ser� desativado </b>  \r\n" + 
				"                            </p>\r\n" + 
				"\r\n" + 
				"                          <tr>\r\n" + 
				"                              \r\n" + 
				"                            <td class=\"body-text-cell\">\r\n" + 
				"                             Para continuar com o cadastro clique em  continuar.\r\n" + 
				"                            </td>\r\n" + 
				"                          </tr>\r\n" + 
				"                        </table>\r\n" + 
				"                        <div><a href='"+link+"'\r\n" + 
				"                        style=\"background-color:#41CC00;background-image:url(https://www.filepicker.io/api/file/N8GiNGsmT6mK6ORk00S7);border:1px solid #407429;border-radius:4px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:17px;font-weight:bold;text-shadow: -1px -1px #47A54B;line-height:38px;text-align:center;text-decoration:none;width:230px;-webkit-text-size-adjust:none;mso-hide:all;\">Continuar</a></div>\r\n" + 
				"                        <table class=\"body-signature-block\">\r\n" + 
				"                          <tr>\r\n" + 
				"                           \r\n" + 
				"                          </tr>\r\n" + 
				"                        </table>\r\n" + 
				"                      </td>\r\n" + 
				"                      <td class=\"body-padding\"></td>\r\n" + 
				"                    </tr>\r\n" + 
				"                  </table>\r\n" + 
				"                </td>\r\n" + 
				"              </tr>\r\n" + 
				"            </table>\r\n" + 
				"          </div>\r\n" + 
				"        </td>\r\n" + 
				"        <td></td>\r\n" + 
				"      </tr>\r\n" + 
				"    </table>\r\n" + 
				"\r\n" + 
				"    <table class=\"footer-wrap w320 full-width-gmail-android\" bgcolor=\"#e5e5e5\">\r\n" + 
				"      <tr>\r\n" + 
				"        <td></td>\r\n" + 
				"        <td class=\"container\">\r\n" + 
				"          <div class=\"content footer-lead\">\r\n" + 
				"          \r\n" + 
				"          </div>\r\n" + 
				"        </td>\r\n" + 
				"        <td></td>\r\n" + 
				"      </tr>\r\n" + 
				"    </table>\r\n" + 
				"    <table class=\"footer-wrap w320 full-width-gmail-android\" bgcolor=\"#e5e5e5\">\r\n" + 
				"      <tr>\r\n" + 
				"        <td></td>\r\n" + 
				"       \r\n" + 
				"        <td></td>\r\n" + 
				"      </tr>\r\n" + 
				"    </table>\r\n" + 
				"  </div>\r\n" + 
				"\r\n" + 
				"</body>\r\n" + 
				"</html>\r\n" + 
				"\r\n" + 
				"";
		
		return msg;
	}

}
