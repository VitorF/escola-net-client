package br.com.escolanet.util;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import org.omnifaces.util.Faces;

import br.com.secretaria.usuario.Usuario;

public class AutenticacaoListener implements PhaseListener {


	private static final long serialVersionUID = 1L;

	@Override
	public void afterPhase(PhaseEvent arg0) {
		
		String paginaAtual = Faces.getViewId();
		boolean isPaginaPublica  = AutenticacaoListener.paginasPublicas().contains(paginaAtual);
		
		if(!isPaginaPublica) {
			Usuario usuarioLogado = (Usuario)	SessionContext.getInstance().getAttribute("usuarioLogado");
			
			if(usuarioLogado == null){
				try {
					FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		}
		
	
			
		
	}

	@Override
	public void beforePhase(PhaseEvent arg0) {
	
	
		
	}

	@Override
	public PhaseId getPhaseId() {
		// TODO Auto-generated method stub
		return PhaseId.RESTORE_VIEW;
	}

	
	
	private static List<String> paginasPublicas(){
		
		String [] vetor = {"/login.xhtml","/index.xhtml","/cadastroToken.xhtml","/cadastroUsuario.xhtml"};
		return Arrays.asList(vetor);
	}
}
