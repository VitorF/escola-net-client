package br.com.escolanet.util;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailUtil {
	
	
	public static void enviar(String email , String assunto , String msg  ) {
	Properties props = new Properties();
    /** Par�metros de conex�o com servidor Gmail */
	props.put("mail.smtp.auth", "true");
	props.put("mail.smtp.starttls.enable", "true");
	props.put("mail.smtp.host", "smtp.gmail.com");
	props.put("mail.smtp.port", "587");
 
    Session session = Session.getDefaultInstance(props,
      new javax.mail.Authenticator() {
           protected PasswordAuthentication getPasswordAuthentication() 
           {
                 return new PasswordAuthentication("escolanetma@gmail.com", 
                 "escolanet1234");
           }
      });
 
    /** Ativa Debug para sess�o */
    //session.setDebug(true);
 
    try {
 
      Message message = new MimeMessage(session);
      message.setFrom(new InternetAddress(email)); 
      //Remetente
 
      Address[] toUser = InternetAddress //Destinat�rio(s)
                 .parse(email);  
 
      message.setRecipients(Message.RecipientType.TO, toUser);
      message.setSubject(assunto);//Assunto
      message.setContent(msg,  "text/html");
      /**M�todo para enviar a mensagem criada*/
      Transport.send(message);
 
      System.out.println("Feito!!!");
 
     } catch (MessagingException e) {
        throw new RuntimeException(e);
    }
  }
	


}
