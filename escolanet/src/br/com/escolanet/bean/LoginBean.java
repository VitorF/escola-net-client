package br.com.escolanet.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import br.com.escolanet.util.FacesUtil;
import br.com.escolanet.util.SessionContext;
import br.com.ma.secretaria.funcionario.FuncionarioFacabeBean;
import br.com.secretaria.matricula.Matricula;
import br.com.secretaria.matricula.MatriculaFacadeBean;
import br.com.secretaria.usuario.Usuario;
import br.com.secretaria.usuario.UsuarioFacadeBean;

@ManagedBean
@SessionScoped
public class LoginBean implements Serializable {

	@EJB
	private FuncionarioFacabeBean funcionarioFacabeBean;
	@EJB
	private UsuarioFacadeBean usuarioFacadeBean;
	@EJB
	private MatriculaFacadeBean matriculaFacadeBean;

	private Usuario usuarioLogado;
	private Matricula matriculaSelecionada;
	private List<Matricula> matriculas;

	private String login  =  "614.625.823-70";
	private String senha = "123456";

	public void login() {

		try {

			usuarioLogado = usuarioFacadeBean.login(login);

			if (usuarioLogado == null) {
				throw new RuntimeException("   Login Invalido  ");
			}

			if (!usuarioLogado.getSenha().equals(senha)) {
				throw new RuntimeException("   Senha Invalido  ");

			}

//		matriculaModalId

			matriculas = matriculaFacadeBean.findAllToFuncionario(usuarioLogado.getFuncionario().getFuncionarioId());

			if (matriculas.size() > 1) {
				org.primefaces.context.RequestContext.getCurrentInstance().execute("PF('matriculaModalId').show()");
			} else {
				matriculaSelecionada = matriculas.get(0);
				SessionContext.getInstance().setAttribute("usuarioLogado", usuarioLogado);

				SessionContext.getInstance().setAttribute("matriculaSelecionada",matriculaSelecionada );

				FacesContext.getCurrentInstance().getExternalContext().redirect("modeloDiario.xhtml");
			}

		} catch (Exception e) {
			FacesUtil.addMsgWARN(e.getMessage());

		}

	}
	
	

	public void selecaoMatricula(ActionEvent e) {
		try {
			 matriculaSelecionada = (Matricula) e.getComponent().getAttributes().get("matriculaSelecionada");
			
			System.out.println(matriculaSelecionada);

			SessionContext.getInstance().setAttribute("usuarioLogado", usuarioLogado);

			SessionContext.getInstance().setAttribute("matriculaSelecionada", matriculaSelecionada);

			FacesContext.getCurrentInstance().getExternalContext().redirect("aluno.xhtml");
		} catch (IOException e1) {
			FacesUtil.addMsgWARN(e1.getMessage());
			e1.printStackTrace();
		}
	}

	
	public Matricula getMatriculaSelecionada() {
		return matriculaSelecionada;
	}
	public void logof() {

	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	public List<Matricula> getMatriculas() {
		return matriculas;
	}

	public void setMatriculas(List<Matricula> matriculas) {
		this.matriculas = matriculas;
	}

}
