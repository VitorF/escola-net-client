package br.com.escolanet.bean;

import java.util.Date;
import java.util.UUID;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.escolanet.util.EmailUtil;
import br.com.escolanet.util.FacesUtil;
import br.com.escolanet.util.MsgUtil;
import br.com.ma.secretaria.funcionario.Funcionario;
import br.com.ma.secretaria.funcionario.FuncionarioFacabeBean;
import br.com.secretaria.tokken.TokkenValidacao;
import br.com.secretaria.tokken.TokkenValidacaoFacadeBean;
import br.com.secretaria.usuario.Usuario;
import br.com.secretaria.usuario.UsuarioFacadeBean;

@ManagedBean
@ViewScoped
public class CadastroTokenBean {

	@EJB
	FuncionarioFacabeBean funcionarioFacabeBean;

	@EJB
	TokkenValidacaoFacadeBean tokkenValidacaoFacadeBean;

	@EJB
	UsuarioFacadeBean usuarioFacadeBean;

	private String cpf;

	private String email;

	public void cadastrar() {

		try {
			Funcionario funcionario = funcionarioFacabeBean.buscaPorCpf(cpf);

			if (funcionario == null) {
				throw new RuntimeException("N�o Possui nenhum funcionario com esse cpf !");
			}

			if (!funcionario.getEmail().trim().equalsIgnoreCase(email.trim())) {
				throw new RuntimeException("O e-mail informado esta incorreto !!");
			}

			Usuario usuario = usuarioFacadeBean.FindToFuncionario(funcionario.getFuncionarioId());

			if (usuario != null) {

				throw new RuntimeException(" Esse Funcionario ja possui um usuario cadastrado ");
			}

			TokkenValidacao tokken = new TokkenValidacao();

			tokken.setFuncionario(funcionario);
			tokken.setDataCriacao(new Date());
			tokken.setCodigo(UUID.randomUUID().toString());

			tokkenValidacaoFacadeBean.salvar(tokken);

			String link = "http://localhost:8080/escolanet/cadastroUsuario.xhtml?tq=" + tokken.getCodigo();

			EmailUtil.enviar(email, "Cadastro de Usuario", MsgUtil.msgCadastro(funcionario, link));

			FacesUtil.addMsgInfor("Link de Cadastro enviado para o email do funcionario !!");

		} catch (Exception e) {
			FacesUtil.addMsgERROR(e.getMessage());
		}
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
