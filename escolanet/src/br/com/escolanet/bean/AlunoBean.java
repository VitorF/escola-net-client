package br.com.escolanet.bean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import br.com.escolanet.util.FacesUtil;
import br.com.escolanet.util.SessionContext;
import br.com.secretaria.aluno.Aluno;
import br.com.secretaria.aluno.AlunoFacadeBean;
import br.com.secretaria.arquivoaluno.ArquivoAluno;
import br.com.secretaria.arquivoaluno.ArquivoAlunoFacadeBean;
import br.com.secretaria.matricula.Matricula;

@ManagedBean
@ViewScoped
public class AlunoBean {

	@EJB
	private AlunoFacadeBean alunoFacadeBean;

	@EJB
	private ArquivoAlunoFacadeBean arquivoAlunoFacadeBean;

	private List<Aluno> alunos;
	private Aluno alunoSelecionado;
	
	private Matricula matriculaSelecionada;

	private List<UploadedFile> uploadedFile;

	private List<ArquivoAluno> arquivosAluno;
	
	
	public AlunoBean() {
		matriculaSelecionada = (Matricula) SessionContext.getInstance().getAttribute("matriculaSelecionada");
	}
	

	@PostConstruct
	public void init() {

		String alunoId = FacesUtil.getParam("alunoId");

		alunos = alunoFacadeBean.findAll();
		uploadedFile = new ArrayList<>();
		arquivosAluno = new ArrayList<>();
		if (alunoId != null) {
			alunoSelecionado = alunoFacadeBean.findByPrimaryKey(Integer.parseInt(alunoId));
		} else {
			alunoSelecionado = new Aluno();

		}

	}

	public void novo() {

	}

	public void handleFileUpload(FileUploadEvent event) throws IOException {

		arquivosAluno.add(new ArquivoAluno(event.getFile().getFileName(), event.getFile().getInputstream()));

	}

	public void removerArquivo(ActionEvent e) {

		ArquivoAluno arquivoAluno = (ArquivoAluno) e.getComponent().getAttributes().get("arquivo");


	}

	public void salvar() {
		try {

			validacaoDados();

			alunoSelecionado.setDataCadastro(new Date());
			alunoSelecionado.setUnidadeCadastro(matriculaSelecionada.getUnidade());

			alunoFacadeBean.update(alunoSelecionado);

			Messages.addGlobalInfo("Sucesso !", null);

		} catch (Exception e) {

			Messages.addGlobalWarn(e.getMessage(), null);

		}

	}

	public void excluirAluno(ActionEvent e) {
		Aluno aluno = (Aluno) e.getComponent().getAttributes().get("alunoSelecionado");

		alunoFacadeBean.remove(aluno.getAlunoId());

		FacesUtil.addMsgInfor("Aluno excluido com sucesso !");

	}

	private void validacaoDados() throws Exception {

		if (alunoSelecionado.getNome() == null) {
			throw new Exception("Informe o Nome do Aluno");
		}

		if (alunoSelecionado.getEndereco() == null) {
			throw new Exception("Informe o Endere�o do Aluno");

		}

	}

	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

	public List<ArquivoAluno> getArquivosAluno() {
		return arquivosAluno;
	}

	public void setArquivosAluno(List<ArquivoAluno> arquivosAluno) {
		this.arquivosAluno = arquivosAluno;
	}

	public Aluno getAlunoSelecionado() {
		return alunoSelecionado;
	}

	public void setAlunoSelecionado(Aluno alunoSelecionado) {
		this.alunoSelecionado = alunoSelecionado;
	}

}
