package br.com.escolanet.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import br.com.escolanet.util.FacesUtil;
import br.com.escolanet.util.SessionContext;
import br.com.ma.secretaria.periodo.Periodo;
import br.com.ma.secretaria.periodo.PeriodoFacadeBean;
import br.com.secretaria.diario.DiarioFacadeBean;
import br.com.secretaria.diarioperiodo.DiarioPeriodo;
import br.com.secretaria.diarioperiodo.DiarioPeriodoFacadeBean;
import br.com.secretaria.matricula.Matricula;
import br.com.secretaria.modelodiario.ModeloDiario;
import br.com.secretaria.modelodiario.ModeloDiarioFacadeBean;
import br.com.secretaria.modeloperiododiario.ModeloPeriodoDiario;
import br.com.secretaria.modeloperiododiario.ModeloPeriodoDiarioFacadeBean;
import br.com.secretaria.turmaaluno.TurmaAlunoFacadeBean;
import br.com.secretaria.turmadisciplina.TurmaDisciplinaFacadeBean;
import br.com.secretaria.usuario.Usuario;

@ManagedBean
@ViewScoped
public class ModeloDiarioMB {

	@EJB
	private ModeloDiarioFacadeBean modeloDiarioFacadeBean;

	@EJB
	private DiarioFacadeBean diarioFacadeBean;

	@EJB
	private ModeloPeriodoDiarioFacadeBean periodoDiarioFacadeBean;

	@EJB
	private DiarioPeriodoFacadeBean diarioPeriodoFacadeBean;

	@EJB
	private PeriodoFacadeBean periodoFacadeBean;

	@EJB
	private TurmaDisciplinaFacadeBean turmaDisciplinaFacadeBean;

	@EJB
	private TurmaAlunoFacadeBean turmaAlunoFacadeBean;

	private List<ModeloDiario> listaModeloDiario;

	private List<ModeloPeriodoDiario> listaModeloPeriodo;

	private List<Periodo> periodos;

	private ModeloDiario modeloDiario;
	private Usuario usuarioLogado;
	private Matricula matriculaSelecionada;
	
	private boolean btnAtualizar = false;
	
	
	
	
	
	
	
	
	
	

	@PostConstruct
	public void create() {
		usuarioLogado = (Usuario) SessionContext.getInstance().getAttribute("usuarioLogado");
		matriculaSelecionada = (Matricula) SessionContext.getInstance().getAttribute("matriculaSelecionada");
		periodos = periodoFacadeBean.findAll();
		reloadModeloDiario();

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	public void novoModeloDiario() {
		btnAtualizar = false;
		modeloDiario = new ModeloDiario();
		listaModeloPeriodo = new ArrayList<>();
		modeloDiario.setUsuarioCriacao(usuarioLogado);
		modeloDiario.setUnidade(this.matriculaSelecionada.getUnidade());
		ModeloPeriodoDiario modeloPeriodoDiario;
		for (Periodo periodo : periodos) {

			modeloPeriodoDiario = new ModeloPeriodoDiario();
			modeloPeriodoDiario.setPeriodo(periodo);

			listaModeloPeriodo.add(modeloPeriodoDiario);
		}

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	public void salvarDiario() {

		modeloDiario.setDataCriacao(new Date());

		if (modeloDiario.getAno() != null) {

			modeloDiarioFacadeBean.salvar(modeloDiario);

			for (ModeloPeriodoDiario modeloDiarioPeriodo : listaModeloPeriodo) {

				modeloDiarioPeriodo.setModeloDiario(modeloDiario);
				periodoDiarioFacadeBean.salvar(modeloDiarioPeriodo);
			}

			modeloDiario = modeloDiarioFacadeBean.findByPrimaryKey(modeloDiario.getModeloDiarioId());

			try {
				diarioFacadeBean.gerarDiariosPorModelo(modeloDiario);
				reloadModeloDiario();
				org.primefaces.context.RequestContext.getCurrentInstance().execute("PF('modalModeloDiario').hide();");
				FacesUtil.addMsgInfor("Diarios Gerados com sucesso !!");
			} catch (Exception e) {
				e.printStackTrace();
				modeloDiarioFacadeBean.remove(modeloDiario.getModeloDiarioId());
				FacesUtil.addMsgERROR("Erro ao gerar Diarios !!");
			}
		} else {
			FacesUtil.addMsgWARN("Informe o ano !!");
		}
	}

	
	
	
	
	
	
	
	
	
	public void excluirModeloDiario(ActionEvent e) {
		try {
			ModeloDiario modeloDiario = (ModeloDiario) e.getComponent().getAttributes().get("modeloDiarioSelecionado");
			modeloDiarioFacadeBean.removeFull(modeloDiario.getModeloDiarioId());
			reloadModeloDiario();
			FacesUtil.addMsgInfor("Modelo e Diarios removidos com sucesso !");
		} catch (Exception ex) {
			ex.printStackTrace();
			FacesUtil.addMsgERROR("Erro ao excluir modelo !");
		}

	}
	
	
	
	
	
	
	
	
	
	

	public void reloadDiarios(ActionEvent e) {
		ModeloDiario modeloDiario = (ModeloDiario) e.getComponent().getAttributes().get("modeloDiarioSelecionado");

		try {

			modeloDiario = modeloDiarioFacadeBean.findByPrimaryKey(modeloDiario.getModeloDiarioId());

			diarioFacadeBean.gerarDiariosPorModelo(modeloDiario);
			

			FacesUtil.addMsgInfor("Lista de Diarios Atualizados com sucesso !");
			
			

		} catch (Exception ex) {
			ex.printStackTrace();

			FacesUtil.addMsgERROR("Erro ao Atualizar lista de Diarios !!");
		}

	}

	public void edit(ActionEvent e) {
		btnAtualizar = true;
		this.modeloDiario = (ModeloDiario) e.getComponent().getAttributes().get("modeloDiarioSelecionado");
		this.listaModeloPeriodo = modeloDiario.getListPeriodos();
	}
	
	
	
	
	
	
	
	
	
	
	
	

	public void atualizarModeloDiario() {
		try {
			validacaoPeriodos();

			for (ModeloPeriodoDiario modeloPeriodoDiario : listaModeloPeriodo) {

				periodoDiarioFacadeBean.alterar(modeloPeriodoDiario);

				List<DiarioPeriodo> diarioPeriodos = diarioPeriodoFacadeBean
						.findPorModeloPeriodoDiario(modeloPeriodoDiario.getModeloPeriodoDiarioId());

				for (DiarioPeriodo diarioPeriodo : diarioPeriodos) {

					diarioPeriodo.setDataInicio(modeloPeriodoDiario.getDataInicio());
					diarioPeriodo.setDataFim(modeloPeriodoDiario.getDataFim());

					diarioPeriodoFacadeBean.alterar(diarioPeriodo);
				}

			}
			reloadModeloDiario();
			
			org.primefaces.context.RequestContext.getCurrentInstance().execute("PF('modalModeloDiario').hide();");

			FacesUtil.addMsgInfor("Informações Atualizadas com sucesso");


		} catch (Exception e) {
			e.printStackTrace();		
			FacesUtil.addMsgERROR(e.getMessage());

		}

	}
	
	
	
	

	private void reloadModeloDiario() {
		this.listaModeloDiario = modeloDiarioFacadeBean
				.finAllToUnidade(this.matriculaSelecionada.getUnidade().getUnidadeId());
	}
	
	
	
	
	
	

	private void validacaoPeriodos() {
		for (ModeloPeriodoDiario modeloPeriodoDiario : listaModeloPeriodo) {

			if (modeloPeriodoDiario.getDataInicio() == null) {
				throw new RuntimeException(
						"Infome  a data inicio do " + modeloPeriodoDiario.getPeriodo().getDescricao());
			}

			if (modeloPeriodoDiario.getDataFim() == null) {
				throw new RuntimeException("Infome  a data Fim do " + modeloPeriodoDiario.getPeriodo().getDescricao());

			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

	public boolean isBtnAtualizar() {
		return btnAtualizar;
	}

	public void setBtnAtualizar(boolean btnAtualizar) {
		this.btnAtualizar = btnAtualizar;
	}

	public List<ModeloDiario> getListaModeloDiario() {
		return listaModeloDiario;
	}

	public void setListaModeloDiario(List<ModeloDiario> listaModeloDiario) {
		this.listaModeloDiario = listaModeloDiario;
	}

	public ModeloDiario getModeloDiario() {
		return modeloDiario;
	}

	public void setModeloDiario(ModeloDiario modeloDiario) {
		this.modeloDiario = modeloDiario;
	}

	public List<ModeloPeriodoDiario> getListaModeloPeriodo() {
		return listaModeloPeriodo;
	}

	public void setListaModeloPeriodo(List<ModeloPeriodoDiario> listaModeloPeriodo) {
		this.listaModeloPeriodo = listaModeloPeriodo;
	}

}
