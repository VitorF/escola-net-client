package br.com.escolanet.bean;

import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.escolanet.util.FacesUtil;
import br.com.ma.secretaria.funcionario.Funcionario;
import br.com.secretaria.tokken.TokkenValidacao;
import br.com.secretaria.tokken.TokkenValidacaoFacadeBean;
import br.com.secretaria.usuario.Usuario;
import br.com.secretaria.usuario.UsuarioFacadeBean;

@ManagedBean
@ViewScoped
public class CadastroUsuarioBean {

	@EJB
	private TokkenValidacaoFacadeBean tokkenValidacaoFacadeBean;

	@EJB
	private UsuarioFacadeBean usuarioFacadeBean;

	private TokkenValidacao token;
	private Usuario usuario;
	private Funcionario funcionario;
	private String codigo;

	private boolean cadastroVisible;

	@PostConstruct
	public void init() {

		verificaTokken();

		if (token != null) {

			Calendar calendarAtual = Calendar.getInstance();
			Calendar calendarToken = Calendar.getInstance();
			calendarAtual.setTime(new Date());
			calendarToken.setTime(token.getDataCriacao());

			int dias = calendarAtual.get(Calendar.DAY_OF_YEAR) - calendarToken.get(Calendar.DAY_OF_YEAR);

			if (dias <= 1) {
				funcionario = token.getFuncionario();
				usuario = new Usuario();
				usuario.setFuncionario(funcionario);
				usuario.setLogin(funcionario.getCpf());
				cadastroVisible = true;
			} else {
				cadastroVisible = false;

			}

		} else {
			cadastroVisible = false;
		}
	}

	public void cadastrar() {

		try {

			if (!usuario.getSenha().equalsIgnoreCase(usuario.getConfirmacaoSenha())) {
				throw new RuntimeException("  A senha n�o foi confirmada corretamente !  ");
			}
			
			token = tokkenValidacaoFacadeBean.findTokkenCodigo(codigo);
			
			if(token == null) {
				throw new IllegalArgumentException(" tokken Invalido !! ");
			}

			usuario.setDataCriacao(new Date());

			usuarioFacadeBean.salvar(usuario);

			token.setValidado(true);
			
			tokkenValidacaoFacadeBean.alterar(token);

			verificaTokken();

			FacesUtil.addMsgInfor(" Usuario criado com sucesso !");
			
			

		} catch(IllegalArgumentException e) {
			
			cadastroVisible = false;
			
		}catch (RuntimeException e) {

			FacesUtil.addMsgWARN(e.getMessage());

		}

	}

	private void verificaTokken() {
		 codigo = FacesUtil.getParam("tq");

		token = tokkenValidacaoFacadeBean.findTokkenCodigo(codigo);

	}

	public TokkenValidacao getToken() {
		return token;
	}

	public void setToken(TokkenValidacao token) {
		this.token = token;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public boolean isCadastroVisible() {
		return cadastroVisible;
	}

	public void setCadastroVisible(boolean cadastroVisible) {
		this.cadastroVisible = cadastroVisible;
	}

}
