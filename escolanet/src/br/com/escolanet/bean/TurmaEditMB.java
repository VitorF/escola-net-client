package br.com.escolanet.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleModel;

import br.com.escolanet.util.FacesUtil;
import br.com.secretaria.aluno.Aluno;
import br.com.secretaria.aluno.AlunoFacadeBean;
import br.com.secretaria.disciplina.Disciplina;
import br.com.secretaria.disciplina.DisciplinaFacadeBean;
import br.com.secretaria.matricula.Matricula;
import br.com.secretaria.matricula.MatriculaFacadeBean;
import br.com.secretaria.serie.SerieFacadeBean;
import br.com.secretaria.turma.Turma;
import br.com.secretaria.turma.TurmaFacadeBean;
import br.com.secretaria.turmaaluno.TurmaAluno;
import br.com.secretaria.turmaaluno.TurmaAlunoFacadeBean;
import br.com.secretaria.turmadisciplina.TurmaDisciplina;
import br.com.secretaria.turmadisciplina.TurmaDisciplinaFacadeBean;

@ManagedBean
@ViewScoped
public class TurmaEditMB {

	@EJB
	private TurmaFacadeBean turmaFacadeBean;

	@EJB
	private TurmaDisciplinaFacadeBean turmaDisciplinaFacadeBean;

	@EJB
	private AlunoFacadeBean alunoFacadeBean;

	@EJB
	private SerieFacadeBean serieFacadeBean;

	@EJB
	private TurmaAlunoFacadeBean turmaAlunoFacadeBean;

	@EJB
	private DisciplinaFacadeBean disciplinaFacadeBean;

	@EJB
	private MatriculaFacadeBean matriculaFacadeBean;

	private List<TurmaDisciplina> turmaDisciplinas = new ArrayList<>();
	private List<TurmaAluno> turmaAlunos = new ArrayList<>();
	private List<Aluno> alunos;
	private List<Aluno> alunosSelecionados;
	private List<Disciplina> disciplinas;
	private List<Matricula> matriculasDisponiveis;

	private TurmaDisciplina turmaDisciplinaCad;
	private TurmaDisciplina turmaDisciplinaSelecionado;
	private Turma turmaSelecionada;
	
	
	private ScheduleModel eventModel;

	@PostConstruct
	public void init() {
		String turmaId = FacesUtil.getParam("turmaId");

		this.turmaSelecionada = turmaFacadeBean.findByPrimaryKeky(Integer.parseInt(turmaId));
		/*
		 * Carregando as Informaçãoes da Turma e suas respectivas referências
		 */
		
		eventModel =   new DefaultScheduleModel();
		
		reloadTurmaDisciplina();
		reloadTurmaAluno();
		reloadAlunosNaoMapeados();

	}

	public void novaTurmaDisciplina() {
		this.turmaDisciplinaCad = new TurmaDisciplina();
		this.disciplinas = disciplinaFacadeBean.findToTipoNilvel(this.turmaSelecionada.getSerie().getTipoNivelEnum());
		System.out.println("Size:" + disciplinas.size());
	}

	public void selecaoProfessoresDisponiveis() {

		if (turmaDisciplinaCad.getDisciplina() != null && turmaSelecionada != null) {
			turmaDisciplinaCad.setTurma(this.turmaSelecionada);
			this.matriculasDisponiveis = matriculaFacadeBean.matriculasDisponiveisTurmaDisciplina(turmaDisciplinaCad);
			System.out.println("Size Matricula:" + this.matriculasDisponiveis.size());
		} else {
			System.out.println("Null");
		}

	}

	public void salvarTurmaDisciplina() {
		try {

			if (this.turmaDisciplinaCad.getDisciplina() == null
					|| this.turmaDisciplinaCad.getDisciplina().getDisciplinaId() == null) {
				throw new RuntimeException("Selecione  a  Disciplina !");
			}

			if (this.turmaDisciplinaCad.getTurma() == null) {
				throw new RuntimeException("Informa a Turma !");
			}

			if (turmaDisciplinaFacadeBean.isPossuiDisciplina(this.turmaDisciplinaCad.getTurma().getTurmaId(),
					this.turmaDisciplinaCad.getDisciplina().getDisciplinaId())) {
				throw new RuntimeException("Essa Disciplina ja foi cadastrada nessa  turma !!");
			}

			turmaDisciplinaFacadeBean.salvar(turmaDisciplinaCad);
			turmaDisciplinaCad = new TurmaDisciplina();
			reloadTurmaDisciplina();

			org.primefaces.context.RequestContext.getCurrentInstance().execute("PF('modalAddTurmaDisciId').hide();");

			FacesUtil.addMsgInfor("Disciplina cadastrado com sucesso");

		} catch (Exception e) {
			e.printStackTrace();
			FacesUtil.addMsgWARN(e.getMessage());
		}
	}

	public void excluirTurmaDisciplina(ActionEvent e) {
		try {
			TurmaDisciplina turmaDisciplina = (TurmaDisciplina) e.getComponent().getAttributes()
					.get("turmaDisciplinaSelecionada");

			System.out.println("ID :" + turmaDisciplina.getTurmaDisciplinaId());

			turmaDisciplinaFacadeBean.remover(turmaDisciplina.getTurmaDisciplinaId());

			reloadTurmaDisciplina();
			FacesUtil.addMsgInfor("Disciplina Excluida com Sucesso");

		} catch (Exception ex) {
			ex.printStackTrace();
			FacesUtil.addMsgERROR(ex.getMessage());
		}

	}

	/*
	 * Tab Turma Aluno
	 */

	public void novoAlunoTurma() {

		reloadAlunosNaoMapeados();

	}

	public void adicionarAluno(ActionEvent e) {

		Aluno aluno = (Aluno) e.getComponent().getAttributes().get("alunoSelecionado");

		TurmaAluno turmaAluno = new TurmaAluno();
		turmaAluno.setAluno(aluno);
		turmaAluno.setTurma(turmaSelecionada);

		turmaAluno.setDataMatricula(new Date());
		turmaAlunoFacadeBean.update(turmaAluno);

		reloadTurmaAluno();
		reloadAlunosNaoMapeados();

		FacesUtil.addMsgInfor(" Aluno adicionado com sucesso !!");

	}

	public void removerTurmaAluno(ActionEvent ev) {
		TurmaAluno turmaAluno = (TurmaAluno) ev.getComponent().getAttributes().get("turmaAlunoSelecionado");

		turmaAlunoFacadeBean.remover(turmaAluno.getTurmaAlunoId());

		reloadTurmaAluno();
		reloadAlunosNaoMapeados();

		FacesUtil.addMsgInfor(" Aluno removido com sucesso !!");

	}

	public List<Disciplina> getDisciplinas() {
		return disciplinas;
	}

	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}

	public List<Matricula> getMatriculasDisponiveis() {
		return matriculasDisponiveis;
	}

	public void setMatriculasDisponiveis(List<Matricula> matriculasDisponiveis) {
		this.matriculasDisponiveis = matriculasDisponiveis;
	}

	public TurmaDisciplina getTurmaDisciplinaCad() {
		return turmaDisciplinaCad;
	}

	public void setTurmaDisciplinaCad(TurmaDisciplina turmaDisciplinaCad) {
		this.turmaDisciplinaCad = turmaDisciplinaCad;
	}

	public void reloadAlunosNaoMapeados() {
		this.alunos = alunoFacadeBean.findAlunosNaoMapeadosNoAno(this.turmaSelecionada.getAno(),
				this.turmaSelecionada.getUnidade().getUnidadeId());
	}

	private void reloadTurmaDisciplina() {
		this.turmaDisciplinas = turmaDisciplinaFacadeBean.findAllTurma(this.turmaSelecionada.getTurmaId());
	}

	private void reloadTurmaAluno() {
		this.turmaAlunos = turmaAlunoFacadeBean.findAlunosTurma(this.turmaSelecionada.getTurmaId());
	}

	public List<TurmaDisciplina> getTurmaDisciplinas() {
		return turmaDisciplinas;
	}

	public void setTurmaDisciplinas(List<TurmaDisciplina> turmaDisciplinas) {
		this.turmaDisciplinas = turmaDisciplinas;
	}

	public Turma getTurmaSelecionada() {
		return turmaSelecionada;
	}

	public void setTurmaSelecionada(Turma turmaSelecionada) {
		this.turmaSelecionada = turmaSelecionada;
	}

	public List<TurmaAluno> getTurmaAlunos() {
		return turmaAlunos;
	}

	public void setTurmaAlunos(List<TurmaAluno> turmaAlunos) {
		this.turmaAlunos = turmaAlunos;
	}

	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

	public List<Aluno> getAlunosSelecionados() {
		return alunosSelecionados;
	}

	public void setAlunosSelecionados(List<Aluno> alunosSelecionados) {
		this.alunosSelecionados = alunosSelecionados;
	}
	
	public ScheduleModel getEventModel() {
		return eventModel;
	}
	
	public void setEventModel(ScheduleModel eventModel) {
		this.eventModel = eventModel;
	}

}
