package br.com.escolanet.bean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.escolanet.util.FacesUtil;
import br.com.escolanet.util.SessionContext;
import br.com.ma.secretaria.funcionario.Funcionario;
import br.com.ma.secretaria.funcionario.FuncionarioFacabeBean;
import br.com.secretaria.diario.Diario;
import br.com.secretaria.diario.DiarioFacadeBean;
import br.com.secretaria.diario.FiltroDiario;
import br.com.secretaria.disciplina.Disciplina;
import br.com.secretaria.disciplina.DisciplinaFacadeBean;
import br.com.secretaria.matricula.Matricula;
import br.com.secretaria.matricula.MatriculaFacadeBean;
import br.com.secretaria.serie.Serie;
import br.com.secretaria.serie.SerieFacadeBean;
import br.com.secretaria.turno.TurnoEnum;
import br.com.secretaria.usuario.Usuario;

@ManagedBean
@ViewScoped
public class ListaDiarioMB {

	@EJB
	private DiarioFacadeBean diarioFacadeBean;

	@EJB
	private FuncionarioFacabeBean funcionarioFacabeBean;

	@EJB
	private SerieFacadeBean serieFacadeBean;

	@EJB
	private MatriculaFacadeBean matriculaFacadeBean;
	
	@EJB
	private DisciplinaFacadeBean disciplinaFacadeBean;

	private List<Diario> diarios;
	private List<TurnoEnum> turnos;
	private List<Serie> series;
	private List<Funcionario> professores;
	private List<Disciplina> disciplinas;

	private Matricula matriculaLogada;
	private FiltroDiario filtroDiario;

	@PostConstruct
	public void init() {
		
		matriculaLogada = (Matricula) SessionContext.getInstance().getAttribute("matriculaSelecionada");
		
		diarios = new ArrayList<>();
		series = serieFacadeBean.findAll();
        turnos = Arrays.asList(TurnoEnum.values());
        disciplinas = new ArrayList<>(); 
		
		reloadFiltro();

	}

	public void pesquisar() {
		try {
		
		this.diarios = diarioFacadeBean.busca(filtroDiario);
		
		}catch (Exception e) {
			FacesUtil.addMsgERROR(e.getMessage());
		}

	}
	
	
	public void reloadDisciplina() {
		
		if(filtroDiario.getSerie() != null ) {
			disciplinas = disciplinaFacadeBean.findToTipoNilvel(filtroDiario.getSerie().getTipoNivelEnum());
		}else {
			disciplinas = new ArrayList<>();
			filtroDiario.setDisciplina(null);
		}
		
	
	}

	private void reloadFiltro() {
		filtroDiario = new FiltroDiario(matriculaLogada.getUnidade());
		if (matriculaLogada.isAcessoDirecao()) {
			System.out.println("if");
			professores = matriculaFacadeBean.findProfessorToUnidade(matriculaLogada.getUnidade().getUnidadeId());
		} else {
			System.out.println("else");
			filtroDiario.setMatricula(matriculaLogada);
		}
	}

	public List<Diario> getDiarios() {
		return diarios;
	}

	public void setDiarios(List<Diario> diarios) {
		this.diarios = diarios;
	}

	public List<TurnoEnum> getTurnos() {
		return turnos;
	}

	public void setTurnos(List<TurnoEnum> turnos) {
		this.turnos = turnos;
	}

	public List<Serie> getSeries() {
		return series;
	}

	public void setSeries(List<Serie> series) {
		this.series = series;
	}

	public List<Funcionario> getProfessores() {
		return professores;
	}

	public void setProfessores(List<Funcionario> professores) {
		this.professores = professores;
	}

	public FiltroDiario getFiltroDiario() {
		return filtroDiario;
	}

	public void setFiltroDiario(FiltroDiario filtroDiario) {
		this.filtroDiario = filtroDiario;
	}

	public List<Disciplina> getDisciplinas() {
		return disciplinas;
	}

	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}
	
	
	
	

}
