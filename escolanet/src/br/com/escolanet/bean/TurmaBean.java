package br.com.escolanet.bean;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import br.com.escolanet.util.FacesUtil;
import br.com.escolanet.util.SessionContext;
import br.com.secretaria.disciplina.Disciplina;
import br.com.secretaria.disciplina.DisciplinaFacadeBean;
import br.com.secretaria.matricula.Matricula;
import br.com.secretaria.serie.Serie;
import br.com.secretaria.serie.SerieFacadeBean;
import br.com.secretaria.turma.Turma;
import br.com.secretaria.turma.TurmaFacadeBean;
import br.com.secretaria.turmadisciplina.TurmaDisciplina;
import br.com.secretaria.turmadisciplina.TurmaDisciplinaFacadeBean;
import br.com.secretaria.turno.TurnoEnum;

@ManagedBean
@ViewScoped
public class TurmaBean {

	@EJB
	private TurmaFacadeBean turmaFacadeBean;
	@EJB
	private SerieFacadeBean serieFacadeBean;
	@EJB
	private TurmaDisciplinaFacadeBean turmaDisciplinaFacadeBean;
	@EJB
	private DisciplinaFacadeBean disciplinaFacadeBean;

	private Turma turmaSelecionda;
	private Matricula matriculaSelecionada;
	private boolean btnEditar;

	private List<Turma> turmas;
	private List<TurnoEnum> turnos;
	private List<Serie> series;

	public TurmaBean() {
		
		matriculaSelecionada  =  (Matricula) SessionContext.getInstance().getAttribute("matriculaSelecionada");
		turnos = Arrays.asList(TurnoEnum.values());

	}

	@PostConstruct
	public void init() {

		series = serieFacadeBean.findAll();
		reloadTurma();
		turmaSelecionda = new Turma();

	}

	public void novo() {
		turmaSelecionda = new Turma();
		btnEditar =  false;
		
	}

	public void salvar() {
		try {

			validacao();

			turmaSelecionda.setDataCriacao(new Date());
			
			turmaSelecionda.setUnidade(matriculaSelecionada.getUnidade());

			Turma turma = turmaFacadeBean.update(turmaSelecionda);

			/*
			 * Criando as Turma Disciplina basedo na turma 
			 */
			
			List<Disciplina> disciplinas = disciplinaFacadeBean.findToTipoNilvel(turma.getSerie().getTipoNivelEnum());

			TurmaDisciplina turmaDisciplina ;
			for (Disciplina disciplina : disciplinas) {
				
				turmaDisciplina  =  new TurmaDisciplina();
				turmaDisciplina.setDisciplina(disciplina);
				turmaDisciplina.setTurma(turma);
			
				
				turmaDisciplinaFacadeBean.salvar(turmaDisciplina);
			}
			
			/*
			 * 
			 */
			
			reloadTurma();

			turmaSelecionda = new Turma();
			FacesUtil.addMsgInfor(" sucesso !!");
		} catch (Exception e) {
			FacesUtil.addMsgWARN(e.getMessage());
		}
	}

	
	
	
	public void atualizarTurma() {
		try {
			validacao();


			turmaFacadeBean.update(turmaSelecionda);

			reloadTurma();

			turmaSelecionda = new Turma();
			FacesUtil.addMsgInfor(" sucesso !!");

		} catch (Exception e) {
			FacesUtil.addMsgWARN(e.getMessage());

		}
	}

	public void removerTurma(ActionEvent e) {
		Turma turma = (Turma) e.getComponent().getAttributes().get("turmaSelecionada");

		turmaFacadeBean.remover(turma.getTurmaId());

		reloadTurma();

		FacesUtil.addMsgInfor("Turma excluida com sucesso !!");

	}

	public void editar(ActionEvent e) {
		this.turmaSelecionda = (Turma) e.getComponent().getAttributes().get("turmaSelecionada");
        this.btnEditar = true;
	}

	private void validacao() throws Exception {
		if (turmaSelecionda.getSerie() == null) {
			throw new Exception("Informe a Serie !!");
		}

		if (turmaSelecionda.getTurno() == null) {
			throw new Exception("Informe a Turno !!");

		}
	}
	
	private void reloadTurma() {
		turmas = turmaFacadeBean.buscaPorUnidade(this.matriculaSelecionada.getUnidade().getUnidadeId());
	}
	
	
	
	
	
	
	//Getts  e Setts

	public Turma getTurmaSelecionda() {
		return turmaSelecionda;
	}

	public void setTurmaSelecionda(Turma turmaSelecionda) {
		this.turmaSelecionda = turmaSelecionda;
	}

	public List<Turma> getTurmas() {
		return turmas;
	}

	public void setTurmas(List<Turma> turmas) {
		this.turmas = turmas;
	}

	public List<TurnoEnum> getTurnos() {
		return turnos;
	}

	public void setTurnos(List<TurnoEnum> turnos) {
		this.turnos = turnos;
	}

	public List<Serie> getSeries() {
		return series;
	}

	public void setSeries(List<Serie> series) {
		this.series = series;
	}
	
	public boolean isBtnEditar() {
		return btnEditar;
	}
	
	public void setBtnEditar(boolean btnEditar) {
		this.btnEditar = btnEditar;
	}

}
