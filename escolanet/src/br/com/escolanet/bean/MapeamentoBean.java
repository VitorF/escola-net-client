package br.com.escolanet.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import br.com.escolanet.util.FacesUtil;
import br.com.escolanet.util.SessionContext;
import br.com.secretaria.disciplina.Disciplina;
import br.com.secretaria.disciplina.DisciplinaFacadeBean;
import br.com.secretaria.matricula.Matricula;
import br.com.secretaria.matricula.MatriculaFacadeBean;
import br.com.secretaria.serie.Serie;
import br.com.secretaria.serie.SerieFacadeBean;
import br.com.secretaria.turma.Turma;
import br.com.secretaria.turma.TurmaFacadeBean;
import br.com.secretaria.turmadisciplina.FiltroTurmaDisciplina;
import br.com.secretaria.turmadisciplina.TurmaDisciplina;
import br.com.secretaria.turmadisciplina.TurmaDisciplinaFacadeBean;

@ManagedBean
@ViewScoped
public class MapeamentoBean {

	@EJB
	private DisciplinaFacadeBean disciplinaFacadeBean;
	@EJB
	private TurmaFacadeBean turmaFacadeBean;
	@EJB
	private TurmaDisciplinaFacadeBean turmaDisciplinaFacadeBean;
	@EJB
	private MatriculaFacadeBean matriculaFacadeBean;
	@EJB
	private SerieFacadeBean serieFacadeBean;

	private List<TurmaDisciplina> turmaDisciplimas;
	private List<Disciplina> disciplinas;
	private List<Matricula> matriculas;
	private List<Serie> series;

	private Integer ano;
	private Matricula matriculaSelecionada;
	private FiltroTurmaDisciplina filtroTurmaDisciplina;
	private TurmaDisciplina turmaDisciplinaSelecionada;
	private Matricula matriculaMapeamento;

	public MapeamentoBean() {
		matriculaSelecionada = (Matricula) SessionContext.getInstance().getAttribute("matriculaSelecionada");
		filtroTurmaDisciplina = new FiltroTurmaDisciplina();
	}
	
	

	@PostConstruct
	public void init() {
		disciplinas = disciplinaFacadeBean.findAll();
		matriculas = matriculaFacadeBean
				.findMatriculasAtivasUnidadeProfessor(matriculaSelecionada.getUnidade().getUnidadeId());
		series = serieFacadeBean.findAll();
	}
	
	
	public void limpaFiltro() {
		filtroTurmaDisciplina = new FiltroTurmaDisciplina();
		turmaDisciplimas =  new ArrayList<>();
		
	}
	

	public void pesquisar() {
		turmaDisciplimas = turmaDisciplinaFacadeBean.filtro(filtroTurmaDisciplina);
	}
	
	

	public void editar(ActionEvent e) {

		this.turmaDisciplinaSelecionada = (TurmaDisciplina) e.getComponent().getAttributes()
				.get("turmaDisciplinaSelecionada");
		
		this.matriculas = matriculaFacadeBean.matriculasDisponiveisTurmaDisciplina(turmaDisciplinaSelecionada);
		
		matriculaMapeamento = null;

	}
	
	
	
	
	public void mapearProfessor() {

		try {
			
			if(matriculaMapeamento == null || matriculaMapeamento.getMatriculaId() == null) {
				
				throw new RuntimeException(" Informe o professor para a Disciplina selecionada !");
			}
			
			
			this.turmaDisciplinaSelecionada.setMatricula(matriculaFacadeBean.findPrimaryKey(matriculaMapeamento.getMatriculaId()));
			
			turmaDisciplinaFacadeBean.update(turmaDisciplinaSelecionada);
			
			
			org.primefaces.context.RequestContext.getCurrentInstance().execute("PF('modalMapeamentoId').hide();");
			
            FacesUtil.addMsgInfor(" Professor(a) "+matriculaMapeamento.getFuncionario().getNome()+" mapeado(a)   com sucesso !!");
            
            pesquisar();

		} catch (Exception e) {
            FacesUtil.addMsgERROR(e.getMessage());
		}

	}
	
	
	
	
	
	public void restauracaoMapeamento(ActionEvent e) {
		
		TurmaDisciplina turmaDisciplina = (TurmaDisciplina) e.getComponent().getAttributes().get("turmaDisciplinaSelecionada");
     
		turmaDisciplina.setMatricula(null);
		
		turmaDisciplinaFacadeBean.update(turmaDisciplina);
		
		 FacesUtil.addMsgInfor(" Restauração realozada  com sucesso !!");
         
         pesquisar();
	   
	}
	
	
	
	
	
	
	
	
	
	public Matricula getMatriculaMapeamento() {
		return matriculaMapeamento;
	}
	
	public void setMatriculaMapeamento(Matricula matriculaMapeamento) {
		this.matriculaMapeamento = matriculaMapeamento;
	}

	public TurmaDisciplina getTurmaDisciplinaSelecionada() {
		return turmaDisciplinaSelecionada;
	}

	public void setTurmaDisciplinaSelecionada(TurmaDisciplina turmaDisciplinaSelecionada) {
		this.turmaDisciplinaSelecionada = turmaDisciplinaSelecionada;
	}

	public List<Disciplina> getDisciplinas() {
		return disciplinas;
	}

	public void setDisciplinas(List<Disciplina> disciplinas) {
		this.disciplinas = disciplinas;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Matricula getMatriculaSelecionada() {
		return matriculaSelecionada;
	}

	public void setMatriculaSelecionada(Matricula matriculaSelecionada) {
		this.matriculaSelecionada = matriculaSelecionada;
	}

	public List<TurmaDisciplina> getTurmaDisciplimas() {
		return turmaDisciplimas;
	}

	public void setTurmaDisciplimas(List<TurmaDisciplina> turmaDisciplimas) {
		this.turmaDisciplimas = turmaDisciplimas;
	}

	public List<Matricula> getMatriculas() {
		return matriculas;
	}

	public void setMatriculas(List<Matricula> matriculas) {
		this.matriculas = matriculas;
	}

	public List<Serie> getSeries() {
		return series;
	}

	public void setSeries(List<Serie> series) {
		this.series = series;
	}

	public FiltroTurmaDisciplina getFiltroTurmaDisciplina() {
		return filtroTurmaDisciplina;
	}

	public void setFiltroTurmaDisciplina(FiltroTurmaDisciplina filtroTurmaDisciplina) {
		this.filtroTurmaDisciplina = filtroTurmaDisciplina;
	}

}
