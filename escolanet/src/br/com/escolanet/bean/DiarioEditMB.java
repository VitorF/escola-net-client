package br.com.escolanet.bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.escolanet.util.FacesUtil;
import br.com.secretaria.diario.Diario;
import br.com.secretaria.diario.DiarioFacadeBean;
import br.com.secretaria.diarioperiodo.DiarioPeriodo;
import br.com.secretaria.itemdiarioperiodo.ItemDiarioPeriodoFacadeBean;

@ManagedBean
@ViewScoped
public class DiarioEditMB {
	
	@EJB
	private DiarioFacadeBean diarioFacadeBean;
	
	@EJB
	private ItemDiarioPeriodoFacadeBean itemDiarioPeriodoFacadeBean;
	
	private Diario diarioSelecionado;
	
	private DiarioPeriodo bimestre01;
	private DiarioPeriodo bimestre02;
	private DiarioPeriodo bimestre03;
	private DiarioPeriodo bimestre04;
	
	
	@PostConstruct
	public void init() {
		
		String diarioId = FacesUtil.getParam("diarioId");
		this.diarioSelecionado = diarioFacadeBean.findByPrimaryKey(Integer.parseInt(diarioId));
		
	}
	
	
	
	
	
	
	
	private void reloaldDiarioPeriodo() {
		if(this.diarioSelecionado != null) {
			this.bimestre01 = null;
			this.bimestre02 = null;
			this.bimestre03 = null;
			this.bimestre04 = null;
		}
	}
	
	
		
}
